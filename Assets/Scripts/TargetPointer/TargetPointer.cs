﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetPointer : MonoBehaviour
{
    public RectTransform arrowSprite;

    public Transform startPoint;
    public Transform targetPoint;

    public bool inited = false;

    public void Init(Transform target, Transform start)
    {
        arrowSprite.gameObject.SetActive(true);
        startPoint = start;
        targetPoint = target;
        inited = true;
    }

    public void Deinit()
    {
        arrowSprite.gameObject.SetActive(false);
        inited = false;
    }

    private void Update()
    {
        if (!inited)
        {
            arrowSprite.gameObject.SetActive(false);
            return;
        }

        if (targetPoint == null || startPoint == null)
            return;
            
        Vector3 toPosition = targetPoint.position;
        Vector3 fromPosition = startPoint.position;
        Vector3 dir = (toPosition - fromPosition).normalized;
        float angle = Vector3.SignedAngle(dir, Vector3.forward, startPoint.up);
        arrowSprite.eulerAngles = new Vector3(0, 0, angle);
    }
}
