﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class AppLanguageChanger : MonoBehaviour
{
    public LocalizationResources localizationResources;
    public LocalizationView localizationView;

    private string language;

    public Canvas canvas;

    private string currentLanguage;

    private void Start()
    {
        localizationView.onButtonPressed += ChangeLanguage;

        currentLanguage = "English";

        string userLanguage = PlayerPrefs.GetString("CurrentLanguage", Application.systemLanguage.ToString());

        var res = localizationResources.resourcesData.Where(x => x.dataName.ToLower() != userLanguage.ToLower());
        language = res.FirstOrDefault().dataName;

        if (!userLanguage.Equals(language))
            ChangeLanguage(userLanguage);
    }

    [ContextMenu("ChangeLanguage")]
    public void ChangeLanguage()
    {
        ChangeLanguage(language);
    }

    public void ChangeLanguage(string newLanguage)
    {
        Text[] texts = canvas.GetComponentsInChildren<Text>(true);
        Debug.Log(newLanguage + "/" + currentLanguage);
        var resourcesForChange = localizationResources.resourcesData.FirstOrDefault(x => x.dataName.Equals(newLanguage));
        var currentLanguageData = localizationResources.resourcesData.FirstOrDefault(x => x.dataName.Equals(currentLanguage));

        if (resourcesForChange == null || currentLanguageData == null)
            return;

        foreach (var text in texts)
        {
            int currentStringIndex = -1;
            currentStringIndex = currentLanguageData.resourcesArray.IndexOf(text.text);

            if (currentStringIndex != -1)
                text.text = resourcesForChange.resourcesArray[currentStringIndex];
        }

        PlayerPrefs.SetString("CurrentLanguage", newLanguage);

        language = currentLanguage;
        currentLanguage = newLanguage;

        localizationView.ChangeImage(newLanguage);
    }
}
