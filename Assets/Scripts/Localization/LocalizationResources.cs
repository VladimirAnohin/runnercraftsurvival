﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LocalizationResources : ScriptableObject
{
    public List<LocalizationData> resourcesData = new List<LocalizationData>();
    public int elements;

    public string ToJson()
    {
        return JsonUtility.ToJson(this);
    }

    public void FromJson(string json)
    {
        LocalizationResources res = JsonUtility.FromJson<LocalizationResources>(json);
        resourcesData = res.resourcesData;
    }
}

[System.Serializable]
public class LocalizationData
{
    public string dataName;
    public List<string> resourcesArray;
}
