﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class LocalizationView : MonoBehaviour
{
    public event Action onButtonPressed;

    [SerializeField]
    private Sprite[] languageSprites;

    [SerializeField]
    private Image currentLanguageImage;

    private Dictionary<string, Sprite> languageSpritesNamesDictionary;

    private void Awake()
    {
        languageSpritesNamesDictionary = new Dictionary<string, Sprite>();

        foreach (var sprite in languageSprites)
        {
            languageSpritesNamesDictionary.Add(sprite.name, sprite);
        }
    }

    public void ChangeImage(string language)
    {
        if(languageSpritesNamesDictionary == null)
            Awake();

        currentLanguageImage.sprite = languageSpritesNamesDictionary[language.ToLower()];
    }
}
