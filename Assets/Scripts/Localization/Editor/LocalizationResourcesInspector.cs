﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.IO;

[CustomEditor(typeof(LocalizationResources))]
public class LocalizationResourcesInspector : Editor
{
    private LocalizationResources localizationResources;

    private string[] stringFields;
    private string[] dataNameStringFields;

    private int languageCount = 1;

    private LocalizationResources targetObject;

    private bool inited;

    private void OnEnable()
    {
        targetObject = target as LocalizationResources;

        if(targetObject.resourcesData != null)
        {
            dataNameStringFields = new string[targetObject.resourcesData.Count];

            for(int i = 0; i < targetObject.resourcesData.Count; i++)
            {
                dataNameStringFields[i] = targetObject.resourcesData[i].dataName;
            }
        }
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        return;
        GUILayout.BeginVertical();

        languageCount = EditorGUILayout.IntField(languageCount);

        GUILayout.EndVertical();

        GUILayout.BeginHorizontal();

        if (targetObject.resourcesData != null || dataNameStringFields != null)
        {
            for (int i = 0; i < targetObject.resourcesData.Count; i++)
            {
                dataNameStringFields[i] = EditorGUILayout.TextField("Data name", dataNameStringFields[i]);
            }
        }

        GUILayout.EndHorizontal();

        DrawStringsResources();

        GUILayout.BeginHorizontal();

        if(stringFields != null)
        {
            for (int i = 0; i < stringFields.Length; i++)
            {
                stringFields[i] = EditorGUILayout.TextArea(stringFields[i], GUILayout.MaxWidth(EditorGUIUtility.currentViewWidth / stringFields.Length));
            }
        }

        GUILayout.EndHorizontal();

        if (GUILayout.Button("Init languages"))
            InitLanguages();

        if (GUILayout.Button("Add new localization string"))
            AddLocalizationString();

        if (GUILayout.Button("Clear"))
            ClearAllResources();

        if (GUILayout.Button("ToJson"))
            Debug.Log(targetObject.ToJson());


        serializedObject.ApplyModifiedProperties();
        EditorUtility.SetDirty(targetObject);

    }

    private void DrawStringsResources()
    {
        if (targetObject.resourcesData == null || targetObject.resourcesData.Count == 0)
            return;

        for (int i = 0; i < targetObject.elements; i++)
        {
            GUILayout.BeginHorizontal();

            foreach (var data in targetObject.resourcesData)
            {
                if(data != null && data.resourcesArray != null && data.resourcesArray.Count != 0)
                {
                    EditorGUILayout.TextField(data.resourcesArray[i]);
                }
            }

            GUILayout.EndHorizontal();
        }

    }

    private void AddLocalizationString()
    {
        for (int i = 0; i < languageCount; i++)
        {
            targetObject.resourcesData[i].resourcesArray.Add(stringFields[i]);
            targetObject.resourcesData[i].dataName = dataNameStringFields[i];
        }

        targetObject.elements++;
    }

    private void InitLanguages()
    {
        ClearAllResources();

        targetObject.resourcesData = new List<LocalizationData>();

        dataNameStringFields = new string[languageCount];

        for (int i = 0; i < languageCount; i++)
        {
            LocalizationData data = new LocalizationData();
            data.resourcesArray = new List<string>();
            data.dataName = dataNameStringFields[i];

            targetObject.resourcesData.Add(data);
        }

        stringFields = new string[languageCount];
    }

    private void ClearAllResources()
    {
        if(targetObject.resourcesData != null)
        {
            targetObject.resourcesData.Clear();
        }

        targetObject.elements = 0;
    }
}
