﻿
public interface IMenu
{
    void Show();
    void Hide();
    MenuEnum GetMenuType();
}
