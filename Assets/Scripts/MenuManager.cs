﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
    public GameManager gameManager;
    public ECCLoadLevel moreGamesLoader;
    public ECCLoadLevel privacyPolicyLoader;
    public GooglePlayGameServices gameServices;

    public GameObject quitGO;

    [SerializeField]
    private Canvas mainCanvas;

    private MenuView currentMenu;
    private MenuView previousMenu;

    private Dictionary<MenuEnum, MenuView> menuDictionary;

    private bool gamePaused;
    private bool gameStarted;

    private void Awake()
    {
        MenuView[] menuGameObjects = mainCanvas.GetComponentsInChildren<MenuView>(true);

        menuDictionary = new Dictionary<MenuEnum, MenuView>();

        for (int i = 0; i < menuGameObjects.Length; i++)
        {
            menuDictionary.Add(menuGameObjects[i].GetMenuType(), menuGameObjects[i]);

            menuGameObjects[i].onShow += SetCurrentMenu;
        }

        Init();
    }

    public void Init()
    {
        if (PlayerPrefs.GetInt("LevelMode", 0) == 0)
        {
            currentMenu = menuDictionary[MenuEnum.QuitMenu];
            menuDictionary[MenuEnum.MainMenu].Show();
        }
        else if (PlayerPrefs.GetInt("LevelMode", 0) == 1)
        {
            PlayerPrefs.SetInt("LevelMode", 0);
            currentMenu = menuDictionary[MenuEnum.GameModeMenu];
            menuDictionary[MenuEnum.LevelSelectorMenu].Show();
        }


        gameManager.gameController.paused += OnGamePaused;
        gameManager.gameController.onGameStarted += OnGameStarted;
    }

    public void ShowMenu(MenuEnum menu)
    {
        menuDictionary[menu].Show();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (gameStarted)
                return;

            if(currentMenu == menuDictionary[MenuEnum.LevelSelectorMenu])
            {
                previousMenu = menuDictionary[MenuEnum.GameModeMenu];
            }

            if (currentMenu == menuDictionary[MenuEnum.GameModeMenu])
            {
                previousMenu = menuDictionary[MenuEnum.MainMenu];
            }

            if (currentMenu == menuDictionary[MenuEnum.MainMenu])
            {
                previousMenu = menuDictionary[MenuEnum.QuitMenu];
            }

            ShowPreviousMenu();
        }
    }

    public void SetCurrentMenu(MenuView menu)
    {
        HideCurrentMenu();
        previousMenu = currentMenu;
        currentMenu = menu;
    }

    public void HideCurrentMenu()
    {
        if (currentMenu == null)
            return;

        currentMenu.Hide();
    }

    public void ShowPreviousMenu()
    {
        previousMenu.Show();
    }

    private void OnGamePaused(bool paused)
    {
        gamePaused = paused;

        ShowPreviousMenu();
    }

    private void OnGameStarted(bool started)
    {
        if (started)
        {
            gameStarted = started;
            HideCurrentMenu();
            currentMenu = menuDictionary[MenuEnum.PauseMenu];
            ShowMenu(MenuEnum.GameMenu);
            //currentMenu = menuDictionary[MenuEnum.GameMenu];
        }
    }
}

public enum MenuEnum
{
    Unidentified = 0,
    MainMenu = 1,
    ShopMenu = 2,
    GameMenu = 3,
    PauseMenu = 4,
    GameModeMenu = 5,
    LevelSelectorMenu = 6,
    QuitMenu = 7
}
