﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public ECCGameController gameController;

    private IGameMode currentGameMode;

    private GameModesEnum currentMode;

    public void SelectMode(int mode)
    {
        switch ((GameModesEnum)mode)
        {
            case GameModesEnum.ChallengeGame:
                {
                    currentGameMode = new ChallengeModeController();
                    break;
                }
            case GameModesEnum.FreeGame:
                {
                    gameController.StartGame();
                    break;
                }
        }

        StartGame();
    }

    private void StartGame()
    {
        currentGameMode.StartGame();
    }
}

public enum GameModesEnum
{
    FreeGame = 0,
    ChallengeGame = 1
}
