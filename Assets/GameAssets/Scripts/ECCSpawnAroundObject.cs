﻿using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using EndlessCarChase.Types;

public class ECCSpawnAroundObject : MonoBehaviour
{
    public bool spawnHelperObjects = true;

    static ECCGameController gameController;

    [Tooltip("The tag of the object around which other objects are spawned within a limited range")]
    public string spawnAroundTag = "Player";
    internal Transform spawnAroundObject;

    [Tooltip("A toggle that turns spawning on and off. If True, we are spawning objects now")]
    public bool isSpawning = false;

    [System.Serializable]
    public class SpawnGroup
    {
        [Tooltip("A list of all Objects that will be spawned")]
        public Transform[] spawnObjects;

        [Tooltip("The rate at which objects are spawned, in seconds.")]
        public float spawnRate = 5;
        internal float spawnRateCount = 0;
        internal int spawnIndex = 0;

        [Tooltip("The distance at which this object is spawned relative to the spawnAroundObject")]
        public Vector2 spawnObjectDistance = new Vector2(10, 20);
    }

    [Tooltip("An array of spawn groups. These can be enemy cars, pickup items, or obstacle rocks for example")]
    public SpawnGroup[] spawnGroups;

    internal int index;

    private void Start()
    {
        if (gameController == null) gameController = GameObject.FindObjectOfType<ECCGameController>();
    }

    void Update()
    {
        if (spawnAroundObject == null && spawnAroundTag != string.Empty && GameObject.FindGameObjectWithTag(spawnAroundTag)) spawnAroundObject = GameObject.FindGameObjectWithTag(spawnAroundTag).transform;

        if (isSpawning == false) return;

        for (index = 0; index < spawnGroups.Length; index++)
        {
            if (index == 0 && !spawnHelperObjects)
            {
                spawnGroups[index].spawnIndex = spawnGroups[index].spawnObjects.Length - 1;
                spawnGroups[index].spawnRate = 8f;
                spawnGroups[index].spawnObjectDistance = new Vector2(30, 30);
            }

            if (spawnGroups[index].spawnObjects.Length > 0)
            {
                if (spawnGroups[index].spawnRateCount > 0)
                    spawnGroups[index].spawnRateCount -= Time.deltaTime;
                else
                {
                    Spawn(spawnGroups[index].spawnObjects, spawnGroups[index].spawnIndex, spawnGroups[index].spawnObjectDistance);

                    spawnGroups[index].spawnIndex++;

                    if (spawnGroups[index].spawnIndex > spawnGroups[index].spawnObjects.Length - 1) spawnGroups[index].spawnIndex = 0;

                    spawnGroups[index].spawnRateCount = spawnGroups[index].spawnRate;
                }
            }
        }
    }

    public void Spawn(Transform[] spawnArray, int spawnIndex, Vector2 spawnGap)
    {
        if (spawnArray[spawnIndex] == null) return;

        Transform newSpawn = Instantiate(spawnArray[spawnIndex]) as Transform;

        if (spawnAroundObject) newSpawn.position = spawnAroundObject.transform.position;// + new Vector3(Random.Range(-5, 5), 0, Random.Range(-5, 5));

        newSpawn.eulerAngles = Vector3.up * Random.Range(0, 360);
        newSpawn.Translate(Vector3.forward * Random.Range(spawnGap.x, spawnGap.y), Space.Self);

        newSpawn.eulerAngles += Vector3.up * 180;

        //if (spawnAroundObject) newSpawn.position += Vector3.up * spawnAroundObject.position.y;

        RaycastHit hit;

        if (Physics.Raycast(newSpawn.position + Vector3.up * 5, -10 * Vector3.up, out hit, 100, gameController.groundLayer)) newSpawn.position = hit.point;

    }
}