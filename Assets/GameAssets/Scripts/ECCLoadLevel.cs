﻿using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ECCLoadLevel : MonoBehaviour
{
    [Tooltip("How many seconds to wait before loading a level or URL")]
    public float loadDelay = 1;

    [Tooltip("The name of the URL to be loaded")]
    public string urlName = "";

    [Tooltip("The name of the level to be loaded")]
    public string levelName = "";

    [Tooltip("The sound that plays when loading/restarting/etc")]
    public AudioClip soundLoad;

    [Tooltip("The tag of the source object from which sounds play")]
    public string soundSourceTag = "Sound";

    [Tooltip("The source object from which sounds play. You can assign this from the scene")]
    public GameObject soundSource;

    [Tooltip("The animation that plays when we start loading a level")]
    public string loadAnimation;

    [Tooltip("The transition effect that appears when we start loading a level")]
    public Transform transition;

    [Tooltip("Should this button be triggered by clicking?")]
    public bool loadOnClick = false;

    void Start()
    {
        //if (!soundSource && GameObject.FindGameObjectWithTag(soundSourceTag)) soundSource = GameObject.FindGameObjectWithTag(soundSourceTag);

        if (loadOnClick == true) GetComponent<Button>().onClick.AddListener(LoadLevel);

    }

    public void LoadURL()
    {
        Time.timeScale = 1;

        if (soundLoad && soundSource) soundSource.GetComponent<AudioSource>().PlayOneShot(soundLoad);

        Invoke("ExecuteLoadURL", loadDelay);
    }

    void ExecuteLoadURL()
    {
        Application.OpenURL(urlName);
    }

    public void LoadLevel()
    {
        Time.timeScale = 1;

        if (soundSource && soundLoad) soundSource.GetComponent<AudioSource>().PlayOneShot(soundLoad);

        if (transition) Invoke("ShowTransition", loadDelay - 1);

        Invoke("ExecuteLoadLevel", loadDelay);
    }

    public void ShowTransition()
    {
        Instantiate(transition);
    }

    void ExecuteLoadLevel()
    {
        SceneManager.LoadScene(levelName);
    }

    public void RestartLevel()
    {
        GoogleAdmobController.Instance.ShowIntersitial();

        Time.timeScale = 1;

        if (soundSource && soundLoad) soundSource.GetComponent<AudioSource>().PlayOneShot(soundLoad);

        if (transition) Instantiate(transition);

        Invoke("ExecuteRestartLevel", loadDelay);
    }

    public void RestartLevelSelector()
    {
        GoogleAdmobController.Instance.ShowIntersitial();

        Time.timeScale = 1;

        if (soundSource && soundLoad) soundSource.GetComponent<AudioSource>().PlayOneShot(soundLoad);

        if (transition) Instantiate(transition);

        Invoke("RestartWithSelectLevel", loadDelay);
    }

    void ExecuteRestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    void RestartWithSelectLevel()
    {
        PlayerPrefs.SetInt("LevelMode", 1);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
