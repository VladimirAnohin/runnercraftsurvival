﻿using UnityEngine;

public class ECCItem : MonoBehaviour
{
    static ECCGameController gameController;

    [Tooltip("The function that runs when this object is touched by the target")]
    public string touchFunction = "ChangeScore";

    [Tooltip("The parameter that will be passed with the function")]
    public float functionParameter = 100;

    [Tooltip("The tag of the target object that the function will play from")]
    public string functionTarget = "GameController";

    [Tooltip("The effect that is created at the location of this item when it is picked up")]
    public Transform pickupEffect;

    [Tooltip("A random rotation given to the object only on the Y axis")]
    public float randomRotation = 360;

    void Start()
    {
        transform.eulerAngles += Vector3.up * Random.Range(-randomRotation, randomRotation);

        if (gameController == null) gameController = GameObject.FindObjectOfType<ECCGameController>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (gameController.playerObject && other.gameObject == gameController.playerObject.gameObject)
        {
            if (touchFunction != string.Empty)
            {
                GameObject.FindGameObjectWithTag(functionTarget).SendMessage(touchFunction, functionParameter);
            }

            if (pickupEffect) Instantiate(pickupEffect, transform.position, transform.rotation);

            Destroy(gameObject);
        }
    }
}
