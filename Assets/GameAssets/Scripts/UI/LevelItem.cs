﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;

public class LevelItem : MonoBehaviour, IPointerClickHandler
{
    public Text currentLvlIndex;

    public Action<int> onClick;

    public void Init(int index, Action<int> onClick)
    {
        currentLvlIndex.text = index.ToString();

        this.onClick = onClick;
    }

    public void OnClick(int index)
    {
        if (onClick != null)
            onClick(index);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        OnClick(int.Parse(currentLvlIndex.text));
    }
}
