﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuView : MonoBehaviour, IMenu
{
    private bool inited = false;

    [SerializeField]
    private MenuEnum menuType;

    public bool IsInited { get { return inited; } }

    public event Action<MenuView> onShow;

    public MenuEnum GetMenuType()
    {
        return menuType;
    }

    public virtual void Hide()
    {
        gameObject.SetActive(false);
    }

    public virtual void Show()
    {
        gameObject.SetActive(true);

        if (onShow != null)
            onShow(this);

        Init();
    }

    protected virtual void Update()
    {
    }

    protected virtual void Init()
    {
        if (inited)
            return;

        inited = true;
    }
}
