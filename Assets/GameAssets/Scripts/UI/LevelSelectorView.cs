﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelectorView : MenuView
{
    //TODO Убрать ссылку нахер отсюда и сделать нормально
    public ChallengeModeController challengeModeController; 

    [SerializeField]
    private Transform content;

    [SerializeField]
    private LevelItem item;

    private List<LevelItem> items;

    public void Start()
    {
        if(items == null)
        {
            items = new List<LevelItem>();

            for (int i = 0; i < challengeModeController.modeLevelInfos.Length; i++)
            {
                var currentItem = Instantiate(item, content);

                if (!challengeModeController.modeLevelInfos[i].levelPassed)
                {
                    currentItem.GetComponent<Button>().interactable = false;
                    currentItem.Init(i + 1, null);
                }
                else
                {
                    currentItem.Init(i + 1, challengeModeController.OnLevelSelected);
                }

                items.Add(currentItem);
            }
        }
    }
}
