﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyMobile;

public class GooglePlayGameServices : MonoBehaviour
{
    void Awake()
    {
        // Init EM runtime if needed (useful in case only this scene is built).
        if (!RuntimeManager.IsInitialized())
            RuntimeManager.Init();
    }

    private void Start()
    {
        Init();
    }

    public void Init()
    {
        if (GameServices.IsInitialized())
        {
            Debug.Log("The module is already initialized.");
        }
        else
        {
            GameServices.Init();
        }
    }

    public void ShowLeaderboard()
    {
        GameServices.ShowLeaderboardUI(EM_GameServicesConstants.Leaderboard_Event_victory);
    }
}
