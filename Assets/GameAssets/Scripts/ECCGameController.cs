﻿using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using EndlessCarChase.Types;
using EasyMobile;
using System;

public class ECCGameController : MonoBehaviour
{
    [Tooltip("The camera object and the camera holder that contains it and follows the player")]
    public Transform cameraHolder;

    public MenuManager menuManager;

    internal Transform miniMap;

    internal float cameraRotate = 0;

    public bool isNeedScore = true;

    public event Action<bool> paused;
    public event Action<bool> onGameStarted;

    [Tooltip("The player object assigned from the project folder or from the shop")]
    public ECCCar playerObject;
    internal float playerDirection;

    [Tooltip("The ground object that follows the player position and gives a feeling of movement.")]
    public Transform groundObject;

    [Tooltip("The layer which the cars move up and down along terrain. A 'groundObject' must not be assigned or hidden  in order to make this work")]
    public LayerMask groundLayer;

    [Tooltip("The speed at which the texture of the ground object moves, make the player seems as if it is moving on the ground")]
    public float groundTextureSpeed = -0.4f;

    [Tooltip("How long to wait before starting the game. Ready? GO! time")]
    public float startDelay = 1;
    internal bool gameStarted = false;

    [Tooltip("The effect displayed before starting the game")]
    public Transform readyGoEffect;

    [Tooltip("The score of the player")]
    public int score = 0;

    public int levelReward = 0;

    [Tooltip("How many points we get per second")]
    public int scorePerSecond = 1;

    [Tooltip("The score text object which displays the current score of the player")]
    public Transform scoreText;

    [Tooltip("Text that is appended to the score value. We use this to add a money sign to the score")]
    public string scoreTextSuffix = "$";

    [Tooltip("The player prefs record of the total score we have ( not high score, but total score we collected in all games which is used as money )")]
    public string moneyPlayerPrefs = "Money";

    internal int highScore = 0;
    internal int scoreMultiplier = 1;

    [Tooltip("The canvas menu that displays the shop where we can unlock and select cars")]
    public ECCShop shopMenu;

    [Tooltip("The steering wheel canvas that allows you to control the player by dragging the wheel left/right")]
    public Slider steeringWheel;

    public Transform gameCanvas;
    public Transform healthCanvas;
    public Transform pauseCanvas;
    public Transform gameOverCanvas;
    public Transform exitAppCanvas;
    public Transform shopCanvas;
    public Transform canvasMenu;

    internal bool isGameOver = false;

    public string mainMenuLevelName = "StartMenu";

    public AudioClip soundGameOver;
    public string soundSourceTag = "GameController";
    internal GameObject soundSource;

    public string confirmButton = "Submit";

    public string pauseButton = "Cancel";
    internal bool isPaused = false;

    internal int index = 0;

    //public Transform slowMotionEffect; // Should dying happen in slow motion?

    internal Vector2 gameArea = new Vector2(10, 10);
    internal bool wrapAroundGameArea = false;

    void Awake()
    {
        Time.timeScale = 1;

        if (shopMenu)
        {
            shopMenu.currentItem = PlayerPrefs.GetInt(shopMenu.currentPlayerprefs, shopMenu.currentItem);

            playerObject = shopMenu.items[shopMenu.currentItem].itemIcon.GetComponent<ECCCar>();
        }

    }

    void Start()
    {
        //Application.targetFrameRate = 30;

        ChangeScore(0);

        //Hide the cavases
        if (shopMenu) shopMenu.gameObject.SetActive(false);
        if (gameOverCanvas) gameOverCanvas.gameObject.SetActive(false);
        if (pauseCanvas) pauseCanvas.gameObject.SetActive(false);
        if (gameCanvas) gameCanvas.gameObject.SetActive(false);

        highScore = PlayerPrefs.GetInt(SceneManager.GetActiveScene().name + "HighScore", 0);

        if (GameObject.FindGameObjectWithTag(soundSourceTag)) soundSource = GameObject.FindGameObjectWithTag(soundSourceTag);

        if (steeringWheel) steeringWheel.gameObject.SetActive(false);
    }

    public void StartGame()
    {
        if (playerObject)
        {
            playerObject = Instantiate(playerObject);

            playerObject.tag = "Player";
        }

        if (GetComponent<ECCSpawnAroundObject>()) GetComponent<ECCSpawnAroundObject>().isSpawning = true;

        if (gameCanvas) gameCanvas.gameObject.SetActive(true);

        if (readyGoEffect)
        {
            var rge = Instantiate(readyGoEffect, FindObjectOfType<Canvas>().transform);
            rge.gameObject.SetActive(true);
        }

        gameStarted = true;

        if (onGameStarted != null)
            onGameStarted(gameStarted);

        if (scorePerSecond > 0) InvokeRepeating("ScorePerSecond", startDelay, 1);

        if (steeringWheel && Application.isMobilePlatform) steeringWheel.gameObject.SetActive(true);

    }

    void Update()
    {
        if (gameStarted == false) return;

        if (startDelay > 0)
        {
            startDelay -= Time.deltaTime;
        }
        else
        {
            if (isGameOver == true)
            {
                if (Input.GetButtonDown(confirmButton))
                {
                    Restart();
                }

                if (Input.GetButtonDown(pauseButton))
                {
                    MainMenu();
                }
            }
            else
            {
                if (playerObject)
                {
                    if (Application.isMobilePlatform)
                    {
                        if (steeringWheel)
                        {
                            if (Input.GetMouseButton(0))
                            {
                                playerDirection = steeringWheel.value;
                            }
                            else
                            {
                                steeringWheel.value = playerDirection = 0;
                            }

                            //steeringWheel.transform.Find("Wheel").eulerAngles = Vector3.forward * playerDirection * -100;
                        }
                        else if (Input.GetMouseButton(0))
                        {
                            if (Input.mousePosition.x > Screen.width * 0.5f)
                            {
                                playerDirection = 1;
                            }
                            else
                            {
                                playerDirection = -1;
                            }
                        }
                        else
                        {
                            playerDirection = 0;
                        }
                    }
                    else
                    {
                        playerDirection = Input.GetAxis("Horizontal");
                    }

                    playerObject.Rotate(playerDirection);

                    if (wrapAroundGameArea == true)
                    {
                        if (playerObject.transform.position.x > gameArea.x * 0.5f) playerObject.transform.position -= Vector3.right * gameArea.x;
                        if (playerObject.transform.position.x < gameArea.x * -0.5f) playerObject.transform.position += Vector3.right * gameArea.x;
                        if (playerObject.transform.position.z > gameArea.y * 0.5f) playerObject.transform.position -= Vector3.forward * gameArea.y;
                        if (playerObject.transform.position.z < gameArea.y * -0.5f) playerObject.transform.position += Vector3.forward * gameArea.y;
                    }
                }

                if (Input.GetButtonDown(pauseButton))
                {
                    if (isPaused == true) Unpause();
                    else Pause(true);
                }
            }
        }
    }

    void LateUpdate()
    {
        if (playerObject)
        {
            if (cameraHolder)
            {
                cameraHolder.position = playerObject.transform.position;

                if (cameraRotate > 0) cameraHolder.eulerAngles = Vector3.up * Mathf.LerpAngle(cameraHolder.eulerAngles.y, playerObject.transform.eulerAngles.y, Time.deltaTime * cameraRotate);
            }

            if (miniMap) miniMap.position = playerObject.transform.position;

            if (groundObject)
            {
                groundObject.position = playerObject.transform.position;

                groundObject.GetComponent<Renderer>().material.SetTextureOffset("_MainTex", new Vector2(playerObject.transform.position.x, playerObject.transform.position.z) * groundTextureSpeed);
            }
        }
    }

    public void ChangeHealth(float changeValue)
    {
        if (playerObject) playerObject.ChangeHealth(changeValue);
    }

    public void ChangeScore(int changeValue)
    {
        score += changeValue;

        if (scoreText && isNeedScore)
        {
            scoreText.GetComponent<Text>().text = score.ToString();

            if (scoreText.GetComponent<Animation>()) scoreText.GetComponent<Animation>().Play();
        }
        else
        {
            scoreText.GetComponent<Text>().gameObject.SetActive(false);
        }
    }

    void SetScoreMultiplier(int setValue)
    {
        scoreMultiplier = setValue;
    }

    public void ScorePerSecond()
    {
        ChangeScore(scorePerSecond);
    }

    public void Pause(bool showMenu)
    {
        isPaused = true;
        OnPause(isPaused);

        Time.timeScale = 0;
    }


    public void Unpause()
    {
        isPaused = false;

        OnPause(isPaused);

        Time.timeScale = 1;
    }

    private void OnPause(bool paused)
    {
        if (this.paused != null)
            this.paused(paused);
    }

    IEnumerator GameOver(float delay)
    {
        isGameOver = true;

        yield return new WaitForSeconds(delay);

        if (pauseCanvas) pauseCanvas.gameObject.SetActive(false);
        if (gameCanvas) gameCanvas.gameObject.SetActive(false);

        int totalMoney = PlayerPrefs.GetInt(moneyPlayerPrefs, 0);

        if (levelReward != 0)
        {
            if (playerObject)
                score = levelReward;
            else
                score = 0;
        }

        totalMoney += score;

        if(GameServices.IsInitialized() && levelReward == 0)
            GameServices.ReportScore(score, EM_GameServicesConstants.Leaderboard_Event_victory);

        PlayerPrefs.SetInt(moneyPlayerPrefs, totalMoney);

        if (gameOverCanvas)
        {
            gameOverCanvas.gameObject.SetActive(true);

            if (levelReward == 0)
            {
                gameOverCanvas.Find("Base/TextScore").GetComponent<Text>().text = score.ToString();

                if (score > highScore)
                {
                    highScore = score;

                    PlayerPrefs.SetInt(SceneManager.GetActiveScene().name + "HighScore", score);
                }

                gameOverCanvas.Find("Base/TextHighScore").GetComponent<Text>().text = highScore.ToString();
            }
            else
            {
                gameOverCanvas.Find("Base/TextLevelInfo").GetComponent<Text>().gameObject.SetActive(true);
                gameOverCanvas.Find("Base/TextLevelInfoTitle").GetComponent<Text>().gameObject.SetActive(true);
                gameOverCanvas.Find("Base/TextHighScore").GetComponent<Text>().gameObject.SetActive(false);
                gameOverCanvas.Find("Base/TextScore").GetComponent<Text>().gameObject.SetActive(false);
                gameOverCanvas.Find("Base/TextHighScore (1)").GetComponent<Text>().gameObject.SetActive(false);
                gameOverCanvas.Find("Base/TextScore (1)").GetComponent<Text>().gameObject.SetActive(false);
                gameOverCanvas.Find("Base/ButtonRestart").gameObject.SetActive(false);
                gameOverCanvas.Find("Base/ButtonSelectLevel").gameObject.SetActive(true);

                if (playerObject)
                    gameOverCanvas.Find("Base/TextLevelInfo").GetComponent<Text>().text = levelReward.ToString() + " $";
                else
                    gameOverCanvas.Find("Base/TextLevelInfo").GetComponent<Text>().text = 0 + " $";
            }


            if (soundSource && soundGameOver)
            {
                soundSource.GetComponent<AudioSource>().pitch = 1;

                soundSource.GetComponent<AudioSource>().PlayOneShot(soundGameOver);
            }
        }

        levelReward = 0;
    }

    public void QuitApp()
    {
        Application.Quit();
    }

    void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    void RestartWithSelectLevel()
    {
        PlayerPrefs.SetInt("LevelMode", 1);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }


    void MainMenu()
    {
        SceneManager.LoadScene(mainMenuLevelName);
    }
}
