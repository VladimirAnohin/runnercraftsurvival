﻿using UnityEngine;

public class ECCObstacle : MonoBehaviour
{
    [Tooltip("The damage caused by this obstacle")]
    public float damage = 1;

    [Tooltip("The effect that is created at the location of this object when it is hit")]
    public Transform hitEffect;

    [Tooltip("Should this obstacle be removed when hit by a car?")]
    public bool removeOnHit = false;

    [Tooltip("A random rotation given to the object only on the Y axis")]
    public float randomRotation = 360;

    void Start()
    {
        transform.eulerAngles += Vector3.up * Random.Range(-randomRotation, randomRotation);

        //InvokeRepeating("ResetColor", 0, 0.5f);
    }

    void OnTriggerStay(Collider other)
    {
        if (other.GetComponent<ECCCar>())
        {
            //if ( other.GetComponent<ECCCar>().hurtDelayCount <= 0 )
            //{
            //other.GetComponent<ECCCar>().hurtDelayCount = other.GetComponent<ECCCar>().hurtDelay;

            other.GetComponent<ECCCar>().ChangeHealth(-damage);

            if (other.GetComponent<ECCCar>().health - damage > 0 && other.GetComponent<ECCCar>().hitEffect) Instantiate(other.GetComponent<ECCCar>().hitEffect, transform.position, transform.rotation);
            //}

            if (hitEffect) Instantiate(hitEffect, transform.position, transform.rotation);

            if (removeOnHit == true) Destroy(gameObject);
        }
    }

    public void ResetColor()
    {
        GetComponent<MeshRenderer>().material.SetColor("_EmissionColor", Color.black);
    }
}