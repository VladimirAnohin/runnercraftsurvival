﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChallengeModeController : MonoBehaviour, IGameMode
{
    public ECCGameController gameController;
    public GameMenu gameMenu;
    public ChallengeModeLevelInfo[] modeLevelInfos;
    public LevelSelectorView levelSelector;
    public ChallengeModeTargetPoint targetPointPrefab;
    public ECCSpawnAroundObject spawnAroundObject;

    private ChallengeModeTargetPoint currentPoint;
    private ChallengeModeLevelInfo currentLevelInfo;

    public TargetPointer targetPointer;

    private int targetPointIndex = 0;
    private int currentLevelIndex = 0;

    private void Start()
    {
        int levelIndex = 0;
        foreach (var levelInfo in modeLevelInfos)
        {
            int passed = PlayerPrefs.GetInt("LevelPassed" + levelIndex, 0);

            if(levelIndex == 0)
            {
                levelInfo.levelPassed = true;
            }
            else
            {
                levelInfo.levelPassed = Convert.ToBoolean(passed);
            }
            levelIndex++;
        }
    }

    public void GameOver()
    {
        throw new System.NotImplementedException();
    }

    public void Pause()
    {
        throw new System.NotImplementedException();
    }

    public void RestartGame()
    {
        throw new System.NotImplementedException();
    }

    public void StartGame()
    {
        Debug.Log("start game");
    }

    public void Unpause()
    {
        throw new System.NotImplementedException();
    }

    public void OnLevelSelected(int selectedLevel)
    {
        spawnAroundObject.spawnHelperObjects = false;
        gameController.StartGame();
        gameController.isNeedScore = false;
       // gameMenu.Show();

        currentLevelInfo = modeLevelInfos[selectedLevel - 1];
        currentLevelIndex = selectedLevel - 1;

        currentPoint = Instantiate(targetPointPrefab);
        currentPoint.transform.position = currentLevelInfo.targetPoints[targetPointIndex];

        targetPointer.Init(currentPoint.transform, gameController.playerObject.transform);

        currentPoint.onPlayerEnter += OnPlayerReachPoint;
        gameController.levelReward = currentLevelInfo.reward;
    }

    private void OnPlayerReachPoint()
    {
        targetPointer.Deinit();
        currentPoint = null;

        if(targetPointIndex + 1 < currentLevelInfo.targetPoints.Length)
        {
            targetPointIndex++;
            currentPoint = Instantiate(targetPointPrefab);
            currentPoint.transform.position = currentLevelInfo.targetPoints[targetPointIndex];
            targetPointer.Init(currentPoint.transform, gameController.playerObject.transform);
            currentPoint.onPlayerEnter += OnPlayerReachPoint;
            gameController.playerObject.GetComponent<ECCCar>().ChangeHealth(2);
        }
        else
        {
            PlayerPrefs.SetInt("LevelPassed" + (currentLevelIndex + 1), 1);
            currentLevelInfo = null;
            gameController.SendMessage("GameOver", 0.2f);
        }
    }
}
