﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGameMode
{
    void StartGame();
    void RestartGame();
    void Pause();
    void Unpause();
    void GameOver();
}
