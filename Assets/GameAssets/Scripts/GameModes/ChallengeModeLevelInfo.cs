﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ChallengeModeLevelInfo
{
    public Vector3[] targetPoints;

    public int reward;
    public bool levelPassed;
}
