﻿using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using EndlessCarChase.Types;
using System;

public class ECCShop : MenuView
{
    internal ECCGameController gameController;

    internal int moneyLeft = 0;

    public MenuManager menuManager;

    public Transform moneyText;

    public Text priceText;
    public Text selectText;

    public string moneySign = "$";

    public string moneyPlayerprefs = "Money";

    internal int currentItem = 0;

    public string currentPlayerprefs = "CurrentCar";

    public ShopItem[] items;

    internal Transform currentIcon;

    public Transform unlockEffect;

    public Button buttonNextItem;
    public Button buttonPrevItem;
    public Button buttonSelectItem;

    public string changeItemButton = "Horizontal";
    public string selectItemButton = "Submit";
    internal bool buttonPressed = false;

    public GameObject shopCamera;

    public Vector3 itemIconPosition;

    public float itemSpinSpeed = 100;
    internal float itemRotation = 0;

    internal float speedMax = 0;
    internal float rotateSpeedMax = 0;
    internal float healthMax = 0;
    internal float damageMax = 0;

    internal int index;

    public void OnEnable()
    {
        moneyLeft = PlayerPrefs.GetInt(moneyPlayerprefs, moneyLeft);

        moneyText.GetComponent<Text>().text = moneyLeft.ToString() + moneySign;

        if (speedMax == 0 && rotateSpeedMax == 0 && healthMax == 0 && damageMax == 0)
        {
            for (index = 0; index < items.Length; index++)
            {
                if (speedMax < items[index].itemIcon.GetComponent<ECCCar>().speed) speedMax = items[index].itemIcon.GetComponent<ECCCar>().speed;
                if (rotateSpeedMax < items[index].itemIcon.GetComponent<ECCCar>().rotateSpeed) rotateSpeedMax = items[index].itemIcon.GetComponent<ECCCar>().rotateSpeed;
                if (healthMax < items[index].itemIcon.GetComponent<ECCCar>().health) healthMax = items[index].itemIcon.GetComponent<ECCCar>().health;
                if (damageMax < items[index].itemIcon.GetComponent<ECCCar>().damage) damageMax = items[index].itemIcon.GetComponent<ECCCar>().damage;
            }
        }

        currentItem = PlayerPrefs.GetInt(currentPlayerprefs, currentItem);

        ChangeItem(0);
    }

    public void OnDisable()
    {
        if (shopCamera) shopCamera.SetActive(false);

        if (currentIcon)
        {
            itemRotation = currentIcon.eulerAngles.y;

            Destroy(currentIcon.gameObject);
        }
    }

    void Start()
    {
        //PlayerPrefs.DeleteAll();

        if (items.Length <= 0) return;

        if (gameController == null) gameController = GameObject.FindObjectOfType<ECCGameController>();

        buttonNextItem.onClick.AddListener(delegate { ChangeItem(1); });

        buttonPrevItem.onClick.AddListener(delegate { ChangeItem(-1); });

        buttonSelectItem.onClick.AddListener(delegate { StartCoroutine("SelectItem"); });
    }

    void Update()
    {
        if (items.Length <= 0) return;

        if (currentIcon) currentIcon.Rotate(Vector3.up * itemSpinSpeed * Time.deltaTime, Space.World);

        if (buttonPressed == false)
        {
            if (Input.GetAxisRaw(changeItemButton) > 0) buttonNextItem.onClick.Invoke();// ChangeItem(1);
            if (Input.GetAxisRaw(changeItemButton) < 0) buttonPrevItem.onClick.Invoke();// ChangeItem(-1);
            if (Input.GetButtonDown(selectItemButton)) buttonSelectItem.onClick.Invoke();// SelectItem();

            if (Input.GetAxisRaw(changeItemButton) != 0 || Input.GetButton(selectItemButton)) buttonPressed = true;

        }
        else if (Input.GetAxisRaw(changeItemButton) == 0 || Input.GetButtonUp(selectItemButton)) buttonPressed = false;
    }

    public void ChangeItem(int changeValue)
    {
        if (items.Length <= 0) return;

        currentItem += changeValue;

        if (currentItem > items.Length - 1) currentItem = 0;
        else if (currentItem < 0) currentItem = items.Length - 1;

        if (currentIcon)
        {
            itemRotation = currentIcon.eulerAngles.y;

            Destroy(currentIcon.gameObject);
        }

        if (items[currentItem].itemIcon)
        {
            currentIcon = Instantiate(items[currentItem].itemIcon.transform, itemIconPosition, Quaternion.identity) as Transform;

            currentIcon.eulerAngles = Vector3.up * itemRotation;

            if (currentIcon.GetComponent<Animation>()) currentIcon.GetComponent<Animation>().Play();

            if (currentIcon.GetComponent<ECCCar>())
            {
                if (transform.Find("Base/Stats/Speed/Full")) transform.Find("Base/Stats/Speed/Full").GetComponent<Image>().fillAmount = currentIcon.GetComponent<ECCCar>().speed / speedMax;
                if (transform.Find("Base/Stats/RotateSpeed/Full")) transform.Find("Base/Stats/RotateSpeed/Full").GetComponent<Image>().fillAmount = currentIcon.GetComponent<ECCCar>().rotateSpeed / rotateSpeedMax;
                if (transform.Find("Base/Stats/Health/Full")) transform.Find("Base/Stats/Health/Full").GetComponent<Image>().fillAmount = currentIcon.GetComponent<ECCCar>().health / healthMax;
                if (transform.Find("Base/Stats/Damage/Full")) transform.Find("Base/Stats/Damage/Full").GetComponent<Image>().fillAmount = currentIcon.GetComponent<ECCCar>().damage / damageMax;

                if (transform.Find("Base/Stats/Speed/Text")) transform.Find("Base/Stats/Speed/Text").GetComponent<Text>().text = currentIcon.GetComponent<ECCCar>().speed.ToString();
                if (transform.Find("Base/Stats/RotateSpeed/Text")) transform.Find("Base/Stats/RotateSpeed/Text").GetComponent<Text>().text = currentIcon.GetComponent<ECCCar>().rotateSpeed.ToString();
                if (transform.Find("Base/Stats/Health/Text")) transform.Find("Base/Stats/Health/Text").GetComponent<Text>().text = currentIcon.GetComponent<ECCCar>().health.ToString();
                if (transform.Find("Base/Stats/Damage/Text")) transform.Find("Base/Stats/Damage/Text").GetComponent<Text>().text = currentIcon.GetComponent<ECCCar>().damage.ToString();
            }
        }

        items[currentItem].lockState = PlayerPrefs.GetInt(currentIcon.name, items[currentItem].lockState);

        if (items[currentItem].lockState == 1)
        {
            priceText.gameObject.SetActive(false);
            selectText.gameObject.SetActive(true);

            PlayerPrefs.SetInt(currentPlayerprefs, currentItem);
        }
        else
        {
            MeshRenderer meshRenderer = currentIcon.transform.Find("Base/Chasis").GetComponent<MeshRenderer>();

            if(meshRenderer != null)
            {
                for (index = 0; index < meshRenderer.materials.Length; index++)
                {
                    meshRenderer.materials[index].SetColor("_Color", Color.black);
                    meshRenderer.materials[index].SetColor("_EmissionColor", Color.black);
                }
            }

            priceText.gameObject.SetActive(true);
            selectText.gameObject.SetActive(false);
            if (priceText) priceText.text = items[currentItem].price.ToString() + moneySign.ToString();
        }
    }

    public void SelectItem()
    {
        if (items[currentItem].lockState == 1)
        {
            gameController.playerObject = items[currentItem].itemIcon.GetComponent<ECCCar>();
            PlayerPrefs.SetInt("CurrentCharacter", currentItem);
            //gameController.StartGame();

            Destroy(currentIcon.gameObject);

            menuManager.ShowMenu(MenuEnum.MainMenu);
        }
        else if (moneyLeft >= items[currentItem].price)
        {
            items[currentItem].lockState = 1;

            PlayerPrefs.SetInt(currentIcon.name, items[currentItem].lockState);

            moneyLeft -= items[currentItem].price;

            PlayerPrefs.SetInt(moneyPlayerprefs, moneyLeft);

            moneyText.GetComponent<Text>().text = moneyLeft.ToString() + moneySign;

            if (unlockEffect) Instantiate(unlockEffect, currentIcon.position, currentIcon.rotation);

            ChangeItem(0);
        }
    }

    public void CloseShop()
    {
        menuManager.ShowMenu(MenuEnum.MainMenu);
    }

    public override void Show()
    {
        base.Show();

        if (shopCamera) shopCamera.SetActive(true);
    }

    public override void Hide()
    {
        base.Hide();

        if (shopCamera) shopCamera.SetActive(false);
    }
}
