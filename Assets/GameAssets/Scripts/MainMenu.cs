﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.Events;

public class MainMenu : MenuView
{
    public GoogleAdmobController admob;
    public Text moneyText;

    [SerializeField]
    private Button[] buttons;

    [SerializeField]
    private UnityEvent[] buttonsEvents;

    private bool onMoneyUpdate;

    private void OnEnable()
    {
        admob.onPlayerGetReward += OnMoneyUpdate;
        moneyText.text = PlayerPrefs.GetInt("Money", 0).ToString() + "$";
    }

    protected override void Init()
    {
        if (IsInited)
            return;

        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].onClick.AddListener(buttonsEvents[i].Invoke);
        }

        base.Init();
    }

    void OnMoneyUpdate()
    {
        onMoneyUpdate = true;
    }

    protected override void Update()
    {
        if (onMoneyUpdate)
        {
            onMoneyUpdate = false;
            UpdateMoney();
        }
    }

    void UpdateMoney()
    {
        int money = PlayerPrefs.GetInt("Money", 0);
        PlayerPrefs.SetInt("Money", money + 100);
        moneyText.text = PlayerPrefs.GetInt("Money", 0).ToString() + "$";
    }

    void OnDisable()
    {
        admob.onPlayerGetReward -= OnMoneyUpdate;
    }
}
