﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChallengeModeTargetPoint : MonoBehaviour
{
    public event Action onPlayerEnter;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (onPlayerEnter != null)
                onPlayerEnter();

            Destroy(this.gameObject);
        }
    }

}
