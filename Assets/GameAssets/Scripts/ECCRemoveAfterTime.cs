﻿using UnityEngine;
using System.Collections;

public class ECCRemoveAfterTime : MonoBehaviour
{
    [Tooltip("How many seconds to wait before removing this object")]
    public float removeAfterTime = 1;

    void Start()
    {
        Destroy(gameObject, removeAfterTime);
    }
}
