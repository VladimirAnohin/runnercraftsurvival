﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ECCCar : MonoBehaviour
{
    internal Transform thisTransform;
    static ECCGameController gameController;
    static Transform targetPlayer;
    internal Vector3 targetPosition;

    internal RaycastHit groundHitInfo;
    internal Vector3 groundPoint;
    internal Vector3 forwardPoint;
    internal float forwardAngle;
    internal float rightAngle;

    [Tooltip("The health of the player. If this reaches 0, the player dies")]
    public float health = 10;
    internal float healthMax;

    internal Transform healthBar;
    internal Image healthBarFill;

    [Tooltip("When the car gets hit and hurt, there is a delay during which it cannot be hit again")]
    public float hurtDelay = 2;
    internal float hurtDelayCount = 0;

    [Tooltip("The color in which the car flashes when hurt")]
    public Color hurtFlashColor = new Color(0.5f, 0.5f, 0.5f, 1);

    [Tooltip("The speed of the player, how fast it moves player. The player moves forward constantly")]
    public float speed = 10;

    [Tooltip("How quickly the player car rotates, in both directions")]
    public float rotateSpeed = 200;
    internal float currentRotation = 0;

    [Tooltip("The damage this car causes when hitting other cars. Damage is reduced from Health.")]
    public int damage = 1;

    [Tooltip("The effect that appears when this car is hit by another car")]
    public Transform hitEffect;

    [Tooltip("The effect that appears when this car dies")]
    public Transform deathEffect;

    [Tooltip("The slight extra rotation that happens to the car as it turns, giving a drifting effect")]
    public float driftAngle = 50;

    [Tooltip("The slight side tilt that happens to the car chassis as the car turns, making it lean inwards or outwards from the center of rotation")]
    public float leanAngle = 10;

    [Tooltip("The chassis object of the car which leans when the car rotates")]
    public Transform chassis;

    [Tooltip("The wheels of the car which rotate based on the speed of the car. The front wheels also rotate in the direction the car is turning")]
    public Transform[] wheels;

    [Tooltip("The front wheels of the car also rotate in the direction the car is turning")]
    public int frontWheels = 2;

    internal int index;

    [Header("AI Car Attributes")]
    [Tooltip("A random value that is added to the base speed of the AI car, to make their movements more varied")]
    public float speedVariation = 2;

    internal float chaseAngle;

    [Tooltip("A random value that is to the chase angle to make the AI cars more varied in how to chase the player")]
    public Vector2 chaseAngleRange = new Vector2(0, 30);

    [Tooltip("Make AI cars try to avoid obstacles. Obstacle are objects that have the ECCObstacle component attached to them")]
    public bool avoidObstacles = true;

    [Tooltip("The width of the obstacle detection area for this AI car")]
    public float detectAngle = 2;

    [Tooltip("The forward distance of the obstacle detection area for this AI car")]
    public float detectDistance = 3;

    //internal float obstacleDetected = 0;

    public float moveHeight = 0;

    private void Start()
    {
        thisTransform = this.transform;

        if (gameController == null) gameController = GameObject.FindObjectOfType<ECCGameController>();
        if (targetPlayer == null && gameController.gameStarted == true && gameController.playerObject) targetPlayer = gameController.playerObject.transform;


        RaycastHit hit;

        if (Physics.Raycast(thisTransform.position + Vector3.up * 5 + thisTransform.forward * 1.0f, -10 * Vector3.up, out hit, 100, gameController.groundLayer)) forwardPoint = hit.point;

        thisTransform.Find("Base").LookAt(forwardPoint);// + thisTransform.Find("Base").localPosition);

        if (gameController.playerObject != this)
        {
            chaseAngle = Random.Range(chaseAngleRange.x, chaseAngleRange.y);

            speed += Random.Range(0, speedVariation);
        }

        if (thisTransform.Find("HealthBar"))
        {
            healthBar = thisTransform.Find("HealthBar");

            healthBarFill = thisTransform.Find("HealthBar/Empty/Full").GetComponent<Image>();
        }

        healthMax = health;

        ChangeHealth(0);
    }

    private void OnValidate()
    {
        frontWheels = Mathf.Clamp(frontWheels, 0, wheels.Length);
    }

    void Update()
    {
        if (gameController && gameController.gameStarted == false) return;

        thisTransform.Translate(Vector3.forward * Time.deltaTime * speed, Space.Self);

        if (health > 0)
        {
            if (targetPlayer) targetPosition = targetPlayer.transform.position;

            if (healthBar) healthBar.LookAt(Camera.main.transform);
        }
        else
        {
            if (healthBar && healthBar.gameObject.activeSelf == true) healthBar.gameObject.SetActive(false);
        }

        if (gameController.playerObject != this)
        {
            //Ray ray = new Ray(thisTransform.position + Vector3.up * 0.2f + thisTransform.right * Mathf.Sin(Time.time * 20) * detectAngle, transform.TransformDirection(Vector3.forward) * detectDistance);

            Ray rayRight = new Ray(thisTransform.position + Vector3.up * 0.2f + thisTransform.right * detectAngle * 0.5f + transform.right * detectAngle * 0.0f * Mathf.Sin(Time.time * 50), transform.TransformDirection(Vector3.forward) * detectDistance);
            Ray rayLeft = new Ray(thisTransform.position + Vector3.up * 0.2f + thisTransform.right * -detectAngle * 0.5f - transform.right * detectAngle * 0.0f * Mathf.Sin(Time.time * 50), transform.TransformDirection(Vector3.forward) * detectDistance);

            RaycastHit hit;

            if (avoidObstacles == true && Physics.Raycast(rayRight, out hit, detectDistance) && (hit.transform.GetComponent<ECCObstacle>() || (hit.transform.GetComponent<ECCCar>() && gameController.playerObject != this)))
            {
                //if (hit.transform.GetComponent<MeshRenderer>() ) hit.transform.GetComponent<MeshRenderer>().material.SetColor("_EmissionColor", Color.red);

                Rotate(-1);

                //obstacleDetected = 0.1f;
            }
            else if (avoidObstacles == true && Physics.Raycast(rayLeft, out hit, detectDistance) && (hit.transform.GetComponent<ECCObstacle>() || (hit.transform.GetComponent<ECCCar>() && gameController.playerObject != this))) // Otherwise, if we detect an obstacle on our left side, swerve to the right
            {
                //if (hit.transform.GetComponent<MeshRenderer>()) hit.transform.GetComponent<MeshRenderer>().material.SetColor("_EmissionColor", Color.red);

                Rotate(1);

                //obstacleDetected = 0.1f;
            }
            else// if (obstacleDetected <= 0) // Otherwise, if no obstacle is detected, keep chasing the player normally
            {
                if (Vector3.Angle(thisTransform.forward, targetPosition - thisTransform.position) > chaseAngle)
                {
                    Rotate(ChaseAngle(thisTransform.forward, targetPosition - thisTransform.position, Vector3.up));
                }
                else 
                {
                    Rotate(0);
                }
            }
        }

        if (gameController.groundObject == null || gameController.groundObject.gameObject.activeSelf == false) DetectGround();

        //if (obstacleDetected > 0) obstacleDetected -= Time.deltaTime;

        if (hurtDelayCount > 0 && health > 0)
        {
            hurtDelayCount -= Time.deltaTime;

            if (GetComponentInChildren<MeshRenderer>())
            {
                foreach (Material part in GetComponentInChildren<MeshRenderer>().materials)
                {
                    if (Mathf.Round(hurtDelayCount * 10) % 2 == 0) part.SetColor("_EmissionColor", Color.black);
                    else part.SetColor("_EmissionColor", hurtFlashColor);

                    //hurtFlashObject.material.SetColor("_EmissionColor", hurtFlashColor);
                }
            }

        }
    }


    public float ChaseAngle(Vector3 forward, Vector3 targetDirection, Vector3 up)
    {
        float approachAngle = Vector3.Dot(Vector3.Cross(up, forward), targetDirection);

        if (approachAngle > 0f)
        {
            return 1f;
        }
        else if (approachAngle < 0f)
        {
            return -1f;
        }
        else
        {
            return 0f;
        }
    }

    public void Rotate(float rotateDirection)
    {
        //thisTransform.localEulerAngles = new Vector3(Quaternion.FromToRotation(Vector3.up, groundHitInfo.normal).eulerAngles.x, thisTransform.localEulerAngles.y, Quaternion.FromToRotation(Vector3.up, groundHitInfo.normal).eulerAngles.z);

        //thisTransform.rotation = Quaternion.FromToRotation(Vector3.up, groundHitInfo.normal);


        if (rotateDirection != 0)
        {
            //thisTransform.localEulerAngles = Quaternion.FromToRotation(Vector3.up, groundHitInfo.normal).eulerAngles + Vector3.up * currentRotation;

            thisTransform.localEulerAngles += Vector3.up * rotateDirection * rotateSpeed * Time.deltaTime;

            thisTransform.eulerAngles = new Vector3(thisTransform.eulerAngles.x, thisTransform.eulerAngles.y, thisTransform.eulerAngles.z);


            currentRotation += rotateDirection * rotateSpeed * Time.deltaTime;

            if (currentRotation > 360) currentRotation -= 360;
            //print(forwardAngle);
            thisTransform.Find("Base").localEulerAngles = new Vector3(rightAngle, Mathf.LerpAngle(thisTransform.Find("Base").localEulerAngles.y, rotateDirection * driftAngle + Mathf.Sin(Time.time * 50) * hurtDelayCount * 50, Time.deltaTime), 0);//  Mathf.LerpAngle(thisTransform.Find("Base").localEulerAngles.y, rotateDirection * driftAngle, Time.deltaTime);

            if (chassis) chassis.localEulerAngles = Vector3.forward * Mathf.LerpAngle(chassis.localEulerAngles.z, rotateDirection * leanAngle, Time.deltaTime);//  Mathf.LerpAngle(thisTransform.Find("Base").localEulerAngles.y, rotateDirection * driftAngle, Time.deltaTime);

            // GetComponent<Animator>().Play("Skid");

            for (index = 0; index < wheels.Length; index++)
            {
                if (index < frontWheels) wheels[index].localEulerAngles = Vector3.up * Mathf.LerpAngle(wheels[index].localEulerAngles.y, rotateDirection * driftAngle, Time.deltaTime * 10);

                wheels[index].Find("WheelObject").Rotate(Vector3.right * Time.deltaTime * speed * 20, Space.Self);
            }
        }
        else 
        {
            thisTransform.Find("Base").localEulerAngles = Vector3.up * Mathf.LerpAngle(thisTransform.Find("Base").localEulerAngles.y, 0, Time.deltaTime * 5);

            if (chassis) chassis.localEulerAngles = Vector3.forward * Mathf.LerpAngle(chassis.localEulerAngles.z, 0, Time.deltaTime * 5);//  Mathf.LerpAngle(thisTransform.Find("Base").localEulerAngles.y, rotateDirection * driftAngle, Time.deltaTime);

            var animator = GetComponent<Animator>();

            if (animator == null)
                animator.GetComponentInChildren<Animator>();

            animator.Play("Move");

            for (index = 0; index < wheels.Length; index++)
            {
                if (index < frontWheels) wheels[index].localEulerAngles = Vector3.up * Mathf.LerpAngle(wheels[index].localEulerAngles.y, 0, Time.deltaTime * 5);

                wheels[index].Find("WheelObject").Rotate(Vector3.right * Time.deltaTime * speed * 30, Space.Self);
            }
        }
    }


    void OnTriggerStay(Collider other)
    {
        if (hurtDelayCount <= 0 && other.GetComponent<ECCCar>())
        {
            hurtDelayCount = hurtDelay;

            other.GetComponent<ECCCar>().ChangeHealth(-damage);

            if (health - damage > 0 && hitEffect) Instantiate(hitEffect, transform.position, transform.rotation);
        }
    }

    public void ChangeHealth(float changeValue)
    {
        health += changeValue;

        if (health > healthMax) health = healthMax;

        if (healthBar)
        {
            healthBarFill.fillAmount = health / healthMax;
        }

        if (changeValue < 0 && gameController.playerObject == this) Camera.main.transform.parent.GetComponent<Animation>().Play();

        if (health <= 0)
        {
            if (gameController.playerObject && gameController.playerObject != this)
            {
                DelayedDie();
            }
            else
            {
                Die();
            }

            if (gameController.playerObject && gameController.playerObject == this)
            {
                gameController.SendMessage("GameOver", 1.2f);

                Time.timeScale = 0.5f;
            }
        }

        if (gameController.playerObject && gameController.playerObject == this && gameController.healthCanvas)
        {
            if (gameController.healthCanvas.Find("Full")) gameController.healthCanvas.Find("Full").GetComponent<Image>().fillAmount = health / healthMax;

            if (gameController.healthCanvas.Find("Text")) gameController.healthCanvas.Find("Text").GetComponent<Text>().text = health.ToString();

            if (gameController.healthCanvas.GetComponent<Animation>()) gameController.healthCanvas.GetComponent<Animation>().Play();
        }
    }

    public void Die()
    {
        if (deathEffect) Instantiate(deathEffect, transform.position, transform.rotation);

        Destroy(gameObject);
    }

    public void DelayedDie()
    {
        //targetPlayer = null;

        for (index = 0; index < wheels.Length; index++)
        {
            wheels[index].transform.SetParent(chassis);
        }

        targetPosition = thisTransform.forward * -10;

        leanAngle = Random.Range(100, 300);

        driftAngle = Random.Range(100, 150); ;

        //rotateSpeed *= 2;

        Invoke("Die", Random.Range(0, 0.8f));
    }

    public void DetectGround()
    {
        Ray carToGround = new Ray(thisTransform.position + Vector3.up * 10, -Vector3.up * 20);

        if (Physics.Raycast(carToGround, out groundHitInfo, 20, gameController.groundLayer))
        {
            //transform.position = new Vector3(transform.position.x, groundHitInfo.point.y, transform.position.z);
        }

        thisTransform.position = new Vector3(thisTransform.position.x, groundHitInfo.point.y + 0.1f, thisTransform.position.z);

        RaycastHit hit;

        if (Physics.Raycast(thisTransform.position + Vector3.up * 5 + thisTransform.forward * 1.0f, -10 * Vector3.up, out hit, 100, gameController.groundLayer))
        {
            forwardPoint = hit.point;
        }
        else if (gameController.groundObject && gameController.groundObject.gameObject.activeSelf == true)
        {
            forwardPoint = new Vector3(thisTransform.position.x, gameController.groundObject.position.y, thisTransform.position.z);
        }

        thisTransform.Find("Base").LookAt(forwardPoint);
    }
}
