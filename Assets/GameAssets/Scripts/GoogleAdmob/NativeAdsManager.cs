﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;
using UnityEngine.UI;
using System.Linq;

public class NativeAdsManager : MonoBehaviour
{

    private UnifiedNativeAd nativeAd;
    private bool unifiedNativeAdLoaded;

    public GameObject nativeAdGO;

    // Below are the UI element that you need to assign from Unity Editor
    public GameObject adChoiceTexture;
    public GameObject appIcon;
    public GameObject headlines;
    public GameObject starRating;
    public GameObject store;
    public GameObject bodyText;
    public GameObject bigImage;
    public GameObject callToAction;
    public GameObject buttonText;


    // Use this for initialization
    void Start()
    {
#if UNITY_ANDROID
        string appId = "ca-app-pub-1134646820220559/9829480689";
#elif UNITY_IPHONE
        string appId = "ca-app-pub-3940256099942544~1458002511";
#else
        string appId = "unexpected_platform";
#endif

        MobileAds.Initialize(appId);

        RequestNativeAd();
    }

    // Update is called once per frame
    void Update()
    {
        if (this.unifiedNativeAdLoaded)
        {
            nativeAdGO.gameObject.SetActive(true);
            this.unifiedNativeAdLoaded = false;

            Texture2D adChoiceLogoTexture = this.nativeAd.GetAdChoicesLogoTexture();
            Debug.Log("get adChoiceLogoTexture  :" + adChoiceLogoTexture);
            if (adChoiceLogoTexture != null)
            {
                adChoiceTexture.GetComponent<RawImage>().texture = adChoiceLogoTexture;
                if (!this.nativeAd.RegisterAdChoicesLogoGameObject(adChoiceTexture))
                {
                    Debug.Log("RegisterAdChoicesLogoGameObject Unsuccessfull");
                }
                adChoiceTexture.AddComponent<BoxCollider>();

            }

            Texture2D iconTexture = this.nativeAd.GetIconTexture();
            Debug.Log("get iconTexture  :" + iconTexture);

            if (iconTexture != null)
            {
                appIcon.GetComponent<RawImage>().texture = iconTexture;
                if (!this.nativeAd.RegisterIconImageGameObject(appIcon))
                {
                    Debug.Log("RegisterIconImageGameObject Unsuccessfull");
                }
                appIcon.AddComponent<BoxCollider>();
            }

            string headline = this.nativeAd.GetHeadlineText();
            Debug.Log("get headline  :" + headline);

            if (headline != null)
            {
                headlines.GetComponent<Text>().text = headline;
                if (!this.nativeAd.RegisterHeadlineTextGameObject(headlines))
                {
                    Debug.Log("RegisterHeadlineTextGameObject Unsuccessfull");
                }
                headlines.AddComponent<BoxCollider>();
            }

            string storeName = this.nativeAd.GetStore();
            Debug.Log("get storeName  :" + storeName);

            if (storeName != null)
            {
                store.GetComponent<Text>().text = storeName;
                if (!this.nativeAd.RegisterStoreGameObject(store))
                {
                    Debug.Log("RegisterStoreGameObject Unsuccessfull");
                }
            }
            else
                store.SetActive(false);

            string bodyText = this.nativeAd.GetBodyText();
            Debug.Log("get bodyText  :" + bodyText);

            if (bodyText != null)
            {
                this.bodyText.GetComponent<Text>().text = bodyText;
                if (!this.nativeAd.RegisterBodyTextGameObject(this.bodyText))
                {
                    Debug.Log("RegisterBodyTextGameObject Unsuccessfull");

                }
                this.bodyText.SetActive(false);
                this.bodyText.AddComponent<BoxCollider>();
            }
            else
                this.bodyText.SetActive(false);

            double starRating = this.nativeAd.GetStarRating();
            Debug.Log("get starRating  :" + starRating);

            if (starRating > 0)
            {
                if (starRating >= 0 && starRating < 2) { this.starRating.transform.GetChild(0).gameObject.SetActive(true); }
                else if (starRating > 1 && starRating < 3) { this.starRating.transform.GetChild(1).gameObject.SetActive(true); }
                else if (starRating > 2 && starRating < 4) { this.starRating.transform.GetChild(2).gameObject.SetActive(true); }
                else if (starRating > 3 && starRating < 5) { this.starRating.transform.GetChild(3).gameObject.SetActive(true); }
                else if (starRating > 4 && starRating < 6) { this.starRating.transform.GetChild(4).gameObject.SetActive(true); }
            }
            else
            {
                this.bodyText.SetActive(true);
                this.starRating.SetActive(false);
            }

            if (this.nativeAd.GetImageTextures().Count > 0)
            {
                List<Texture2D> goList = this.nativeAd.GetImageTextures();

                bigImage.GetComponent<RawImage>().texture = goList[0];
                List<GameObject> list = new List<GameObject>();
                list.Add(bigImage.gameObject);
                this.bigImage.AddComponent<BoxCollider>();



            }
            string buttonTextString = this.nativeAd.GetCallToActionText();
            Debug.Log("get buttonTextString  :" + buttonTextString);

            if (buttonTextString != null)
            {
                buttonText.GetComponent<Text>().text = buttonTextString;
                this.buttonText.AddComponent<BoxCollider>();
            }
            if (!this.nativeAd.RegisterCallToActionGameObject(buttonText))
            {
                Debug.Log("RegisterCallToActionGameObject Unsuccessfull");
            }

            Debug.Log("Headline is " + headline);
            Debug.Log("Advitiser Text is " + this.nativeAd.GetAdvertiserText());

            Debug.Log("GetBodyText is " + this.nativeAd.GetBodyText());
            Debug.Log("GetCallToActionText is " + buttonTextString);

            Debug.Log("GetPrice is " + this.nativeAd.GetPrice());
            Debug.Log("GetStarRating is " + starRating);
            Debug.Log("GetStore is " + storeName);

        }
    }

    private void RequestNativeAd()
    {
        AdLoader adLoader = new AdLoader.Builder("ca-app-pub-3940256099942544/2247696110")
            .ForUnifiedNativeAd()
            .Build();
        adLoader.OnUnifiedNativeAdLoaded += this.HandleUnifiedNativeAdLoaded;
        adLoader.OnAdFailedToLoad += this.HandleNativeAdFailedToLoad;


        adLoader.LoadAd(new AdRequest.Builder().Build());
    }

    private void HandleNativeAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        Debug.Log("Native ad failed to load: " + args.Message);
    }

    private void HandleUnifiedNativeAdLoaded(object sender, UnifiedNativeAdEventArgs args)
    {
        Debug.Log("Unified Native Ad Loaded");
        this.nativeAd = args.nativeAd;
        this.unifiedNativeAdLoaded = true;
    }


}