﻿using GoogleMobileAds.Api;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoogleAdmobController : MonoBehaviour
{
    private static GoogleAdmobController inst;
    public static GoogleAdmobController Instance { get { return inst; } }

    public event Action onPlayerGetReward;

    public Image icon;
    public Text price, store, advertiser;

    private Action onPlayerCloseRewarderAd;
    private Action onPlayerGetRewardFromAd;

    [SerializeField]
    private GDPRView view;

    private InterstitialAd interstitial;
    private BannerView bannerView;
    private UnifiedNativeAd nativeAd;
    private RewardedAd rewardedAd;

    private bool userRewarded;

    private bool inited;
    private bool unifiedNativeAdLoaded;

    private void Awake()
    {
        if (inst == null)
            inst = this;
        else if (inst == this)
            Destroy(gameObject);
    }

    private void Start()
    {
        if (PlayerPrefs.GetInt("PersonalizedAds", 0) == 0)
        {
            view.Show();
            view.onConfirmButtonClick += () => { SetUserGDPRChoice(1); };
            view.onDenimButtonClick += () => { SetUserGDPRChoice(2); };
        }

        InitAdMob();
    }

    void InitAdMob()
    {
        if (inited)
            return;

        inited = true;
        MobileAds.Initialize(initStatus => { });
        RequestInterstitial();
        RequestRewardedAd();
        //RequestNativeAd();
        RequestBanner();
    }

    public void RequestInterstitial()
    {
#if UNITY_ANDROID
        string adUnitId = "ca-app-pub-1134646820220559/5936110352";
#elif UNITY_IPHONE
        string adUnitId = "";
#else
        string adUnitId = "unexpected_platform";
#endif

        interstitial = new InterstitialAd(adUnitId);

        AdRequest request = CreateAdRequest();
        interstitial.LoadAd(request);
    }

    private void Update()
    {
        return;
        if (unifiedNativeAdLoaded)
        {
            icon.gameObject.SetActive(true);

            Texture2D iconTexture = this.nativeAd.GetIconTexture();
            double starRating = nativeAd.GetStarRating();
            string store = nativeAd.GetStore();
            string price = nativeAd.GetPrice();
            string advertiser = nativeAd.GetAdvertiserText();

            this.store.text = store;
            this.price.text = price.ToString();
            this.advertiser.text = advertiser;

            icon.sprite = Sprite.Create(iconTexture, new Rect(0, 0, 128f, 128f), new Vector2());
            unifiedNativeAdLoaded = false;
            if (!nativeAd.RegisterIconImageGameObject(icon.gameObject))
            {
                Debug.Log("Native ad register failed");
            }
        }
    }

    public void RequestRewardedAd()
    {
        this.rewardedAd = new RewardedAd("ca-app-pub-1134646820220559/8671122314");
        this.rewardedAd.OnUserEarnedReward += HandleUserEarnedReward;
        this.rewardedAd.OnAdClosed += HandleRewardedAdClosed;
        AdRequest request = new AdRequest.Builder().Build();
        // Load the rewarded ad with the request.
        this.rewardedAd.LoadAd(request);
    }

    private void RequestBanner()
    {
#if UNITY_ANDROID
        string adUnitId = "ca-app-pub-1134646820220559/2963005223";
#elif UNITY_IPHONE
            string adUnitId = "";
#else
            string adUnitId = "unexpected_platform";
#endif
        float widthInPixels = Screen.safeArea.width > 0 ? Screen.safeArea.width : Screen.width;

        int width = (int)(widthInPixels / MobileAds.Utils.GetDeviceScale());
        AdSize adaptiveSize = AdSize.GetCurrentOrientationAnchoredAdaptiveBannerAdSizeWithWidth(width);

        //AdSize adSize = new AdSize(320, 50);
        bannerView = new BannerView(adUnitId, adaptiveSize, AdPosition.Bottom);

        AdRequest request = CreateAdRequest();

        bannerView.LoadAd(request);
    }

    private void RequestNativeAd()
    {
        AdLoader adLoader = new AdLoader.Builder("ca-app-pub-1134646820220559/9829480689").ForUnifiedNativeAd().Build();
        adLoader.OnUnifiedNativeAdLoaded += HandleUnifiedNativeAdLoaded;
        adLoader.LoadAd(new AdRequest.Builder().Build());
    }

    private void HandleNativeAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        Debug.Log("NATIVEADS");
    }

    private void HandleUnifiedNativeAdLoaded(object sender, UnifiedNativeAdEventArgs args)
    {
        nativeAd = args.nativeAd;
        unifiedNativeAdLoaded = true;
    }

    public void ShowIntersitial()
    {
        if (interstitial.IsLoaded())
        {
            interstitial.Show();
        }
    }

    public void UserChoseToWatchAd()
    {
        if (rewardedAd.IsLoaded())
        {
            rewardedAd.Show();
        }
    }

    private void SetUserGDPRChoice(int value)
    {
        PlayerPrefs.SetInt("PersonalizedAds", value);
    }

    private AdRequest CreateAdRequest()
    {
        int personalizedAds = PlayerPrefs.GetInt("PersonalizedAds", 1);

        if (personalizedAds.Equals(1))
            return new AdRequest.Builder().Build();
        else
            return new AdRequest.Builder().AddExtra("npa", "1").Build();
    }

    private void HandleRewardedAdClosed(object sender, EventArgs args)
    {
        RewardPlayer();
    }

    private void HandleUserEarnedReward(object sender, Reward args)
    {
        userRewarded = true;
    }

    private void RewardPlayer()
    {
        if (userRewarded)
        {
            userRewarded = false;

            if (onPlayerGetReward != null)
                onPlayerGetReward();
        }
    }
}
