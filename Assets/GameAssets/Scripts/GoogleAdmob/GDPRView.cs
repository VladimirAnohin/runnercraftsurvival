﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class GDPRView : MonoBehaviour
{
    public Button confirmButton;
    public Button denimButton;

    public event Action onConfirmButtonClick;
    public event Action onDenimButtonClick;


    public void Hide()
    {
        gameObject.SetActive(false);
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    private void Start()
    {
        confirmButton.onClick.AddListener(OnConfirmButtonClick);
        denimButton.onClick.AddListener(OnDenimButtonClick);
    }

    private void OnConfirmButtonClick()
    {
        if (onConfirmButtonClick != null)
            onConfirmButtonClick();

        Hide();
    }

    private void OnDenimButtonClick()
    {
        if (onDenimButtonClick != null)
            onDenimButtonClick();

        Hide();
    }
}
