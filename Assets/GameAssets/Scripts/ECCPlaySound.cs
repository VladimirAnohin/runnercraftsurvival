﻿using UnityEngine;
using UnityEngine.UI;

public class ECCPlaySound : MonoBehaviour
{
    [Tooltip("The sound to play")]
    public AudioClip sound;

    [Tooltip("The tag of the sound source")]
    public string soundSourceTag = "Sound";

    [Tooltip("Play the sound immediately when the object is activated")]
    public bool playOnStart = true;

    [Tooltip("Play the sound when clicking on this button")]
    public bool playOnClick = false;

    [Tooltip("A random range for the pitch of the audio source, to make the sound more varied")]
    public Vector2 pitchRange = new Vector2(0.9f, 1.1f);

    void Start()
    {
        if (playOnStart == true) PlayCurrentSound();

        // Listen for a click to play a sound
        if (playOnClick && GetComponent<Button>()) GetComponent<Button>().onClick.AddListener(delegate { PlayCurrentSound(); });

    }

    public void PlaySound(AudioClip sound)
    {
        if (soundSourceTag != string.Empty && sound)
        {
            GameObject.FindGameObjectWithTag(soundSourceTag).GetComponent<AudioSource>().pitch = Random.Range(pitchRange.x, pitchRange.y) * Time.timeScale;

            GameObject.FindGameObjectWithTag(soundSourceTag).GetComponent<AudioSource>().PlayOneShot(sound);
        }
        else if (GetComponent<AudioSource>())
        {
            GetComponent<AudioSource>().pitch = Random.Range(pitchRange.x, pitchRange.y) * Time.timeScale;

            GetComponent<AudioSource>().PlayOneShot(sound);
        }

    }

    public void PlayCurrentSound()
    {
        if (soundSourceTag != string.Empty && sound)
        {
            GameObject.FindGameObjectWithTag(soundSourceTag).GetComponent<AudioSource>().pitch = Random.Range(pitchRange.x, pitchRange.y) * Time.timeScale;

            GameObject.FindGameObjectWithTag(soundSourceTag).GetComponent<AudioSource>().PlayOneShot(sound);
        }
        else if (GetComponent<AudioSource>())
        {
            GetComponent<AudioSource>().pitch = Random.Range(pitchRange.x, pitchRange.y) * Time.timeScale;

            GetComponent<AudioSource>().PlayOneShot(sound);
        }
    }
}
