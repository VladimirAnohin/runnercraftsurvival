﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ECCAudioControl : MonoBehaviour
{
    static float currentSoundVolume = 1;

    static float currentMusicVolume = 1;

    void Awake()
    {
        GameObject.FindGameObjectWithTag("Music").GetComponent<AudioSource>().ignoreListenerVolume = true;

        currentMusicVolume = PlayerPrefs.GetFloat("MusicVolume", currentMusicVolume);

        GameObject.FindGameObjectWithTag("Music").GetComponent<AudioSource>().volume = currentMusicVolume;

        currentSoundVolume = PlayerPrefs.GetFloat("SoundVolume", currentSoundVolume);

        AudioListener.volume = currentSoundVolume;

        if (transform.Find("Text"))
        {
            if (transform.Find("Text").GetComponent<Text>().text.Contains("MUSIC"))
            {
                if (currentMusicVolume > 0) transform.Find("Text").GetComponent<Text>().text = "MUSIC:ON";
                else if (currentMusicVolume <= 0) transform.Find("Text").GetComponent<Text>().text = "MUSIC:OFF";
            }
            else if (transform.Find("Text").GetComponent<Text>().text.Contains("SOUND"))
            {
                if (currentSoundVolume > 0) transform.Find("Text").GetComponent<Text>().text = "SOUND:ON";
                else if (currentSoundVolume <= 0) transform.Find("Text").GetComponent<Text>().text = "SOUND:OFF";
            }
        }
    }

    public void ToggleMusic()
    {
        if (currentMusicVolume == 1)
        {
            currentMusicVolume = 0;

            Color newColor = GetComponent<Image>().material.color;
            newColor.a = 0.5f;
            GetComponent<Image>().color = newColor;

            if (transform.Find("Text")) transform.Find("Text").GetComponent<Text>().text = "MUSIC:OFF";
        }
        else
        {
            currentMusicVolume = 1;

            Color newColor = GetComponent<Image>().material.color;
            newColor.a = 1;
            GetComponent<Image>().color = newColor;

            if (transform.Find("Text")) transform.Find("Text").GetComponent<Text>().text = "MUSIC:ON";
        }

        GameObject.FindGameObjectWithTag("Music").GetComponent<AudioSource>().volume = currentMusicVolume;

        PlayerPrefs.SetFloat("MusicVolume", currentMusicVolume);
    }

    public void ToggleSound()
    {
        if (currentSoundVolume == 1)
        {
            currentSoundVolume = 0;

            Color newColor = GetComponent<Image>().material.color;
            newColor.a = 0.5f;
            GetComponent<Image>().color = newColor;

            if (transform.Find("Text")) transform.Find("Text").GetComponent<Text>().text = "SOUND:OFF";
        }
        else
        {
            currentSoundVolume = 1;

            Color newColor = GetComponent<Image>().material.color;
            newColor.a = 1;
            GetComponent<Image>().color = newColor;

            if (transform.Find("Text")) transform.Find("Text").GetComponent<Text>().text = "SOUND:ON";
        }

        AudioListener.volume = currentSoundVolume;

        PlayerPrefs.SetFloat("SoundVolume", currentSoundVolume);
    }
}
