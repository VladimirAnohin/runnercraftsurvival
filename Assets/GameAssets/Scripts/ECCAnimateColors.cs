﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

	public class ECCAnimateColors : MonoBehaviour 
	{
		[Tooltip("A list of the colors that will be animated")]
		public Color[] colorList;
		
		[Tooltip("The index number of the current color in the list")]
		public int colorIndex = 0;
		
		[Tooltip("How long the animation of the color change lasts, and a counter to track it")]
		public float changeTime = 1;
		public float changeTimeCount = 0;
		
		[Tooltip("How quickly the sprite animates from one color to another")]
		public float changeSpeed = 1;
		
		[Tooltip("Is the animation paused?")]
		public bool isPaused = false;
		
		[Tooltip("Is the animation looping?")]
		public bool isLooping = true;

		[Tooltip("Should we start from a random color index")]
		public bool randomColor = false;


		void Start() 
		{
			SetColor();
		}
		
		void Update() 
		{
			if ( isPaused == false )
			{
				if ( changeTime > 0 )
				{
					if ( changeTimeCount > 0 )
					{
						changeTimeCount -= Time.deltaTime;
					}
					else
					{
						changeTimeCount = changeTime;
						
						if ( colorIndex < colorList.Length - 1 )
						{
							colorIndex++;
						}
						else
						{
							if ( isLooping == true )    colorIndex = 0;
						}
					}
				}

                if (GetComponent<Renderer>())
                {
                    GetComponent<Renderer>().material.color = Color.Lerp(GetComponent<Renderer>().material.color, colorList[colorIndex], changeSpeed * Time.deltaTime);
                }

                if ( GetComponent<TextMesh>() )
				{
					GetComponent<TextMesh>().color = Color.Lerp(GetComponent<TextMesh>().color, colorList[colorIndex], changeSpeed * Time.deltaTime);
				}
				
				if ( GetComponent<SpriteRenderer>() )
				{
					GetComponent<SpriteRenderer>().color = Color.Lerp(GetComponent<SpriteRenderer>().color, colorList[colorIndex], changeSpeed * Time.deltaTime);
				}

				if ( GetComponent<Image>() )
				{
					GetComponent<Image>().color = Color.Lerp(GetComponent<Image>().color, colorList[colorIndex], changeSpeed * Time.deltaTime);;
				}
			}
			else
			{
				SetColor();
			}
		}

		void SetColor()
		{
			int tempColor = 0;

			if ( randomColor == true )    tempColor = Mathf.FloorToInt(Random.value * colorList.Length);

            if (GetComponent<Renderer>())
            {
                GetComponent<Renderer>().material.color = colorList[tempColor];
            }

            if ( GetComponent<TextMesh>() )
			{
				GetComponent<TextMesh>().color = colorList[tempColor];
			}
			
			if ( GetComponent<SpriteRenderer>() )
			{
				GetComponent<SpriteRenderer>().color = colorList[tempColor];
			}

			if ( GetComponent<Image>() )
			{
				GetComponent<Image>().color = colorList[tempColor];
			}
		}

		void Pause( bool pauseState )
		{
			isPaused = pauseState;
		}
	}
